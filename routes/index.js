var express = require('express')
var router = express.Router()

var controller = require('../controllers/resortController')

router.get('/search', controller.getAll)
router.get('/search/:id', controller.getId)
router.post('/insert', controller.insert)
router.put('/update/:id', controller.update)
router.delete('/delete/:id', controller.delete)

module.exports = router
