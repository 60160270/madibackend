var resortModel = require('../models/model')

module.exports = {
  getAll: function (req, res) {
    resortModel.find().then(data => {
      res.json(data)
    })
  },
  getId: function (req, res) {
    const { id } = req.params
    console.log({ id })
    console.log(
      resortModel.find({ roomId: id }).then(data => {
        res.json(data)
      })
    )
  },
  insert: function (req, res) {
    const newModel = new resortModel(req.body)
    newModel.save(error => {
      if (error) {
        console.log('Oops , something happened')
      } else {
        console.log('Data has been saved')
      }
    })
    res.json(newModel)
  },
  update: function (req, res) {
    const { id } = req.params
    console.log({ id })

    //update
    resortModel.updateOne({ roomId: id }, req.body, error => {
      if (error) {
        console.log('Oops , something happened')
      } else {
        console.log('Data has been updated')
      }
    })

    //res updated data
    console.log(
      resortModel.find({ roomId: req.body.roomId }).then(data => {
        res.json(data)
      })
    )
  },
  delete: function (req, res) {
    const { id } = req.params
    console.log({ id })

    //update
    resortModel.deleteOne({ roomId: id }, req.body, error => {
      if (error) {
        console.log('Oops , something happened')
      } else {
        console.log('Data has been removed')
      }
    })

    //res all doc
    resortModel.find({}).then(data => {
      res.json(data)
    })
  }
}
