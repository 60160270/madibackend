//mongoose
const mongoose = require('mongoose')

const Schema = mongoose.Schema
const resortSchema = new Schema({
  roomId: String,
  size: Number,
  price: Number
})
const resortModel = mongoose.model('resort_rooms', resortSchema)

module.exports = resortModel
